<?php
/**
 * @file
 * Contains \Drupal\news_pages\Plugin\Block\NewsBlock.
 */
namespace Drupal\news_pages\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides News block.
 *
 * @Block(
 *   id = "news_block",
 *   admin_label = @Translation("News Block"),
 *   category = @Translation("Blocks")
 * )
 */
class NewsBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {

    $header = array(
      array('data' => t('Date'), 'field' => 'created', 'sort' => 'DESC'),
      array('data' => t('Title'),     'field' => 'title'),
    );

    $db = \Drupal::database();

    $query = $db->select('node_field_data', 'n')
      ->addTag('node_access')
      ->condition( 'n.type', 'news_pages' )
      ->condition('status',1);


    $table_sort = $query->extend('Drupal\Core\Database\Query\TableSortExtender')->orderByHeader($header);
    $pager = $table_sort->extend('Drupal\Core\Database\Query\PagerSelectExtender');
    $pager->limit(3);

    $nodes = $pager
      ->fields('n', array('title', 'created', 'nid'))
      ->execute()
      ->fetchAll();


    $list = array();
    foreach ($nodes as $node) {
      $node_entity = entity_load('node', $node->nid);
      $url = $node_entity->toUrl();
      $list[] = \Drupal::l($node->title, $url);
    }

    $item_list = array(
      '#theme' => 'item_list',
      '#items' => $list,
      '#cache' => [
        'max-age' => 0,
      ],
    );
    $render_array = array(
      '#theme' => 'news_pages_block',
      '#content' => drupal_render($item_list),
    );
    return $render_array;
  }
}