<?php
/**
 * @file
 * Contains \Drupal\news_pages\Controller\NewsPagesController.
 */

namespace Drupal\news_pages\Controller;

use Drupal\node\Entity\NodeType;
use Drupal\node\Controller\NodeController;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Component\Utility\SafeMarkup;
use Drupal\Core\Link;
use Drupal\Core\Url;


class NewsPagesController extends ControllerBase {



  /**
   * {@inheritdoc}
   */
  public function add_content() {
    $node_type = NodeType::load('news_pages');
    $node = NodeController::entityManager()->getStorage('node')->create(array(
      'type' => $node_type->id(),
    ));

    $form = NodeController::entityFormBuilder()->getForm($node);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function news_pages_table() {

    $header = array(
      array('data' => t('Title'),     'field' => 'title'),
      array('data' => t('Date'), 'field' => 'created'),
      array('data' => t('Action'),      'field' => 'nid'),
    );

    $db = \Drupal::database();

    $query = $db->select('node_field_data', 'n')
      ->addTag('node_access')
      ->condition( 'n.type', 'news_pages' )
      ->condition('status',1);

    $table_sort = $query->extend('Drupal\Core\Database\Query\TableSortExtender')->orderByHeader($header);
    $pager = $table_sort->extend('Drupal\Core\Database\Query\PagerSelectExtender');
    $pager->limit(10);

    $nodes = $pager
      ->fields('n', array('title', 'created', 'nid'))
      ->execute()
      ->fetchAll();

    $form['mytable'] = array(
      '#type' => 'table',
      '#header' => $header,
//      '#empty' => t('There are no news yet. !add-url', array(
//        '!add-url' => Link::createFromRoute('Add new', 'node.add', array('node_type' => 'news_pages'))
//          ->toString()->getGeneratedLink(),
//      )),
    );

    foreach ($nodes as $node) {
      $node_entity = entity_load('node', $node->nid);
      $url = $node_entity->toUrl();
      $url_edit = Url::fromRoute('entity.node.edit_form', array('node' => $node->nid));
      $url_delete = Url::fromRoute('entity.node.delete_form', array('node' => $node->nid));


      // Some table columns containing raw markup.
      $form['mytable'][$node->title]['label'] = array(
        '#title' => SafeMarkup::checkPlain($node->title),
        '#type' => 'link',
        '#url' => $url,
      );


      // Some table columns containing raw markup.
      $form['mytable'][$node->title]['date'] = array(
        '#plain_text' => format_date($node->created),
      );

      // Operations (dropbutton) column.
      $form['mytable'][$node->title]['operations'] = array(
        '#type' => 'operations',
        '#links' => array(),
      );
      $form['mytable'][$node->title]['operations']['#links']['edit'] = array(
        'title' => t('Edit'),
        'url' => $url_edit,
      );
      $form['mytable'][$node->title]['operations']['#links']['delete'] = array(
        'title' => t('Delete'),
        'url' => $url_delete,
      );
    }

    $form['mytable']['add_new']['label'] = array(
      '#title' => t("Add new..."),
      '#type' => 'link',
      '#url' => Url::fromRoute('node.add', array('node_type' => 'news_pages')),
    );

    $form['pager'] = array(
      '#type' => 'pager'
    );
    return $form;
  }
}
?>
