<?php
/**
 * @file
 * Contains \Drupal\news_pages\Controller\NewsPagesController.
 */

namespace Drupal\news_pages\Controller;

use Drupal\node\Entity\NodeType;
use Drupal\node\NodeTypeInterface;
use Drupal\node\Controller\NodeController;
use Drupal\Core\Controller\ControllerBase;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\field\Entity\FieldConfig;
use Drupal\Component\Utility\SafeMarkup;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\node\Entity\NodeRouteProvider;
/**
 * Adds the default body field to a node type.
 *
 * @param \Drupal\node\NodeTypeInterface $type
 *   A node type object.
 * @param string $label
 *   (optional) The label for field instance.
 * @param string $field_name
 *   (optional) The machine name of field instance.
 * @param string $field_type
 *   (optional) The type of field instance.
 *
 * @return \Drupal\field\Entity\FieldConfig
 *   A Body field object.
 */
function nodetype_add_text_field(NodeTypeInterface $type, $label = 'Body', $field_name = 'body', $field_type = 'text') {
  // Add or remove the body field, as needed.
  $field_storage = FieldStorageConfig::loadByName('node', $field_name);
  if (empty($field_storage)) {
    $field_storage = FieldStorageConfig::create(array(
      'field_name' => $field_name,
      'entity_type' => 'node',
      'type' => $field_type,
    ));
    $field_storage->save();
  }
  $field = FieldConfig::loadByName('node', $type->id(), $field_name);
  if (empty($field)) {
    $field = entity_create('field_config', array(
      'field_storage' => $field_storage,
      'bundle' => $type->id(),
      'label' => $label,
      'settings' => array('display_summary' => $field_type == 'text_with_summary' ? TRUE : FALSE),
    ));
    $field->save();
    switch ($field_type) {
      case 'text':
        $form_field_type = 'text_textfield';
        $display_field_type =  'text_summary_or_trimmed';
        break;
      case 'text_long':
        $form_field_type = 'text_textarea';
        $display_field_type =  'text_summary_or_trimmed';
        break;
      case 'text_with_summary':
        $form_field_type = 'text_textarea_with_summary';
        $display_field_type =  'text_summary_or_trimmed';
        break;
    }
    // Assign widget settings for the 'default' form mode.
    entity_get_form_display('node', $type->id(), 'default')
      ->setComponent($field_name, array(
        'type' => $form_field_type,
      ))
      ->save();
    // Assign display settings for the 'default' and 'teaser' view modes.
    entity_get_display('node', $type->id(), 'default')
      ->setComponent($field_name, array(
//        'label' => 'hidden',
        'type' => 'text_default',
      ))
      ->save();
    // The teaser view mode is created by the Standard profile and therefore
    // might not exist.
    $view_modes = \Drupal::entityManager()->getViewModes('node');
    if (isset($view_modes['teaser'])) {
      entity_get_display('node', $type->id(), 'teaser')
        ->setComponent($field_name, array(
//          'label' => 'hidden',
          'type' => $display_field_type,
        ))
        ->save();
    }
  }
  return $field;
}

class NewsPagesController extends ControllerBase {



  /**
   * {@inheritdoc}
   */
  public function add_content() {
    $node_type = NodeType::load('news_pages');
    $node = NodeController::entityManager()->getStorage('node')->create(array(
      'type' => $node_type->id(),
    ));

    $form = NodeController::entityFormBuilder()->getForm($node);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function news_pages_table() {

    $bundle = NodeType::load('news_pages');
    if (!empty($bundle)) {
      nodetype_add_text_field($bundle, 'Body', 'body', 'text_with_summary');
      nodetype_add_text_field($bundle, 'Alternative Title', 'field_alter_title', 'text');
      nodetype_add_text_field($bundle, 'Admin Comment', 'field_admin_comment', 'text_long');
    }

    $header = array(
      array('data' => t('Title'),     'field' => 'title'),
      array('data' => t('Date'), 'field' => 'created'),
      array('data' => t('Action'),      'field' => 'nid'),
    );

    $db = \Drupal::database();

    $query = $db->select('node_field_data', 'n')
      ->addTag('node_access')
      ->condition( 'n.type', 'news_pages' )
      ->condition('status',1);

//    $count_query = clone $query;
//    $count_query->addExpression('Count(n.nid)');

    $table_sort = $query->extend('Drupal\Core\Database\Query\TableSortExtender')->orderByHeader($header);
    $pager = $table_sort->extend('Drupal\Core\Database\Query\PagerSelectExtender');
    $pager->limit(10);

    $nodes = $pager
      ->fields('n', array('title', 'created', 'nid'))
      ->execute()
      ->fetchAll();

    $form['mytable'] = array(
      '#type' => 'table',
      '#header' => $header,
//      '#empty' => t('There are no news yet. !add-url', array(
//        '!add-url' => Link::createFromRoute('Add new', 'node.add', array('node_type' => 'news_pages'))
//          ->toString()->getGeneratedLink(),
//      )),
    );

    foreach ($nodes as $node) {
      $routes = NodeRouteProvider::getRoutes('node');
      $node_entity = entity_load('node', $node->nid);
      //$url = $node_entity->toUrl();
      $url = Url::fromRoute('entity.node', array('node' => $node->nid));
      $url_edit = Url::fromRoute('entity.node.edit_form', array('node' => $node->nid));
      $url_delete = Url::fromRoute('entity.node.delete_form', array('node' => $node->nid));
//      $test = ConfigEntityListController::getOperations($node_entity);


      // Some table columns containing raw markup.
      $form['mytable'][$node->title]['label'] = array(
        '#title' => SafeMarkup::checkPlain($node->title),
        '#type' => 'link',
        '#url' => $url,
      );


      // Some table columns containing raw markup.
      $form['mytable'][$node->title]['date'] = array(
        '#plain_text' => format_date($node->created),
      );

      // Operations (dropbutton) column.
      $form['mytable'][$node->title]['operations'] = array(
        '#type' => 'operations',
        '#links' => array(),
      );
      $form['mytable'][$node->title]['operations']['#links']['edit'] = array(
        'title' => t('Edit'),
        'url' => $url_edit,
      );
      $form['mytable'][$node->title]['operations']['#links']['delete'] = array(
        'title' => t('Delete'),
        'url' => $url_delete,
      );
    }

    $form['mytable']['add_new']['label'] = array(
      '#title' => t("Add new..."),
      '#type' => 'link',
      '#url' => Url::fromRoute('node.add', array('node_type' => 'news_pages')),
    );

    $form['pager'] = array(
      '#type' => 'pager'
    );
    return $form;
  }
}
?>
